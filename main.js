$(document).on("click", "#btn", function () {
  //   var myModalId = $(this).data("id");
  //   $(".modal-body #modalId").val(myModalId);
  var first = $("#textId1").text();
  var second = $("#textId2").text();

  $("#modalId1").val(first);
  $("#modalId2").val(second);
});

function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
}
